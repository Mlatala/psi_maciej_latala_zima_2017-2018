import java.util.Random;

public class main {

	public static void main(String[] args) {
		
		Random rand = new Random();
		neuron cell = new neuron(2);
		
		for (int i=0;i<cell.getSize();i++){	
			cell.SetInputsValues(i,rand.nextDouble() * 2 - 1);
			cell.SetWeightsValues(i,rand.nextDouble() * 2 - 1);
		}	
			System.out.println("Decyzja:" + cell.Output());	
	}

}
