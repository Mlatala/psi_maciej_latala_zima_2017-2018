import java.util.ArrayList;

public class neuron {

	ArrayList<Double> Inputs;
	ArrayList<Double> Weights;
	
	public neuron(int Inputs_number){
		Inputs = new ArrayList<>(Inputs_number);
		Weights = new ArrayList<>(Inputs_number);
		
		for(int i=0;i<Inputs_number;i++){
			Inputs.add(0.0);
			Weights.add(0.0);			
		}
	} 
	
	public int getSize(){
		return Inputs.size();
	}
	
	public void AddInputs(int n){
		for(int i=0;i<n;i++){
			Inputs.add(0.0);
			Weights.add(0.0);			
		}		
	}
	
	public void SetInputsValues(int index,double value) {
		Inputs.set(index,value);
	}
	
	public double GetInputsValues(int index) {
		return Inputs.get(index);
	}
	
	public void SetWeightsValues(int index,double value) {
		Weights.set(index,value);
	}
	
	public double GetWeightsValues(int index) {
		return Weights.get(index);
	}
	
	public double SummingBox(){
		double sum = 0;
		for (int i=0;i<this.getSize();i++)
			sum+=this.GetInputsValues(i)*this.GetWeightsValues(i);
		
		return sum;
	}
	
	public double ActivationBox(double sum){
		if(sum > 0)
			return 1;
		else
		if(sum < 0)
			return -1;
		else
			return 0;
	}
	
	public double Output(){
		return ActivationBox(SummingBox());
	}
}
