
public class Flower {
    private double petal_data [];
    private int petal_data_n;
    private String species;

    public Flower(double[] petal_data, String species, int how_many_x) {
        this.petal_data = petal_data;
        this.species = species;
        this.petal_data_n = how_many_x;

    }

    public Flower(int petal_data_n){
        this.petal_data = new double[petal_data_n];
        this.petal_data_n = petal_data_n;

    }


    public void setPetal_data_i(double x, int i){
        if (i < petal_data_n)
            this.petal_data[i]=x;
    }

    public double getPetal_data_i(int i){
        if(i<petal_data_n)
            return this.petal_data[i];
        else return 0;
    }

    public double[] getPetal_data() {
        return petal_data;
    }

    public void setPetal_data(double[] petal_data) {
        this.petal_data = petal_data;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getPetal_data_n() {
        return petal_data_n;
    }

    public void setPetal_data_n(int petal_data_n) {
        this.petal_data_n = petal_data_n;
    }

}
