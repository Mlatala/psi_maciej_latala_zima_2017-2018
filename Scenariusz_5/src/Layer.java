import java.util.ArrayList;

public class Layer {

    public Neuron[] neurons;
    private int input_n;
    private int neuron_n;
    private double learn_factor;
    private Flower data;

    public Layer(int neuron_n, int input_n, double learn_factor){

        this.neuron_n = neuron_n;
        this.input_n = input_n;
        this.learn_factor = learn_factor;

        neurons = new Neuron[this.neuron_n];
        for(int i = 0; i< this.neuron_n; i++){
            neurons[i] = new Neuron(this.input_n, this.learn_factor);
        }
    }

    public ArrayList<Double> calculateLayer(Flower input){
        ArrayList<Double> results = new ArrayList<Double>();
        this.data = input;

        for (int i=0 ; i < neurons.length; i++){
            results.add(neurons[i].calculate(input));
        }

        return results;
    }

    public void modify(int id){

        neurons[id].setWeights(data);
    }

    public int getHow_many_neurons() {
        return neuron_n;
    }

    public int getHow_many_x() { return input_n; }
}
