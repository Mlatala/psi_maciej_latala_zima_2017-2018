import java.util.ArrayList;
import java.util.Collections;

public class Network {
	
    public double learn_factor;
    public int neuron_n;
    public int learning_data_n;
    public int test_data_n;
    
    public Flower[] learning_data;
    public Flower[] test_data;

    public Layer layer;

    public Network(){

        learn_factor = 0.05;
        neuron_n = 50;

        layer = new Layer(neuron_n, 4 ,learn_factor);

        ScanData scan_data = new ScanData();

        learning_data =  scan_data.load("learn_data.txt");
        learning_data_n = scan_data.getHow_many_records();

        test_data = scan_data.load("test_data.txt");
        test_data_n = scan_data.getHow_many_records();

    }

    public void learning(){

    normalize(learning_data);
    normalize(test_data);

        int epoka =0;
        ArrayList<Double> result;

        do {

            for (int i = 0; i < learning_data_n; i++) {

                result = layer.calculateLayer(learning_data[i]);
                layer.modify(result.indexOf( Collections.max( result )) );

                result.clear();
            }

            epoka++;

        }while( epoka < 10000 );

    }
    
    public void normalize(Flower[] data){
        for(int i =0 ; i< data.length; i++){
            double lenght = data[i].getPetal_data_i(0)*data[i].getPetal_data_i(0) +
                    data[i].getPetal_data_i(1)*data[i].getPetal_data_i(1) +
                    data[i].getPetal_data_i(2)*data[i].getPetal_data_i(2) +
                    data[i].getPetal_data_i(3)*data[i].getPetal_data_i(3);
            lenght = Math.sqrt(lenght);

            data[i].setPetal_data_i( data[i].getPetal_data_i(0)/lenght, 0 );
            data[i].setPetal_data_i( data[i].getPetal_data_i(1)/lenght, 1 );
            data[i].setPetal_data_i( data[i].getPetal_data_i(2)/lenght, 2 );
            data[i].setPetal_data_i( data[i].getPetal_data_i(3)/lenght, 3 );
        }
    }
    
    public void testing(){
        ArrayList<Double> result;
        ArrayList<Integer> group = new ArrayList<Integer>();
        int winner;
        for(int i=0; i< test_data_n; i++) {

            result = layer.calculateLayer(test_data[i]);


            winner = result.indexOf( Collections.max( result ));

            if(!group.contains(winner)){
                group.add(result.indexOf( Collections.max( result )));
            }



            System.out.println( "Species: " + test_data[i].getSpecies() + "  Group: " + winner );

        }

    }
    
    public void run(){
    	learning();
    	testing();
    }
}
