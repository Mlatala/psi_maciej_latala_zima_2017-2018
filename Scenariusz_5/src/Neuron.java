import java.util.concurrent.ThreadLocalRandom;

public class Neuron {

    private double learn_factor; 
    private double weight[]; 
    private int neuron_n; 

    public Neuron(int neuron_n, double learn_factor){
        this.learn_factor = learn_factor;
        this.neuron_n = neuron_n;

        weight= new double[neuron_n];
        randomizeWeights();
        normalizeWeights();
    }

   public double calculate(Flower data){

       double signal = signalFunction(data);
       return activation_Box(signal);
    }

    private double activation_Box(double signal){
        double result;

        result = signal;

        return result;
    }

    public void setWeights(Flower data) { 
        for (int i = 0; i < neuron_n; i++) {
            weight[i] = weight[i] + learn_factor * (data.getPetal_data_i(i) - weight[i]);
        }

       normalizeWeights();
    }

    private double signalFunction(Flower data){
        double signal = 0;
        for(int i=0; i<neuron_n; i++) {
            signal += data.getPetal_data_i(i) * weight[i];
        }

        return signal;
    }

    private void randomizeWeights(){
        for(int i=0; i<neuron_n; i++)
            weight[i] = ThreadLocalRandom.current().nextDouble(0.01, 0.1);

    }

    private void normalizeWeights(){
        double lenghtSquared = weight[0]*weight[0] + weight[1]*weight[1] + weight[2]*weight[2] + weight[3]*weight[3];
        double lenght = Math.sqrt(lenghtSquared);

        weight[0] /= lenght;
        weight[1] /= lenght;
        weight[2] /= lenght;
        weight[3] /= lenght;
    }

    public double getWeights(int i) {
        return weight[i];
    }

}
