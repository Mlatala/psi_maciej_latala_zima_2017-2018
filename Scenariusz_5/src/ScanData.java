import java.io.*;

public class ScanData {

    private int records;
    private int data_n;

    private InputStream stream;
    private InputStreamReader reader;
    private BufferedReader file;

    public Flower[] load(String filename) {

        String line;
        String parts[];
        Flower data[] = null;

        try {
            stream = new FileInputStream(new File(filename));
            reader = new InputStreamReader(stream);

            file = new BufferedReader(reader);

            line = file.readLine();
            parts = line.split(";");
            records = Integer.parseInt(parts[0]);
            data = new Flower[records];

            line = file.readLine(); 
            parts = line.split(";");
            data_n = Integer.parseInt(parts[0]);

            for (int i = 0; i < records; i++) {
                line = file.readLine();
                parts = line.split(";");
                data[i] = new Flower(data_n);
                for (int j = 0; j < data_n; j++) {
                    data[i].setPetal_data_i(Double.parseDouble(parts[j]), j);
                }
                data[i].setSpecies(parts[data_n]);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

        public int getHow_many_records() {
            return records;
        }

        public int getHow_many_x() {
            return data_n;
        }
}
