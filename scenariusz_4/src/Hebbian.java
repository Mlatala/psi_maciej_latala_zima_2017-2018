import java.io.File;
import java.nio.file.Path;
import java.util.Random;
import java.util.Scanner;

public class Hebbian {
   
	public int learning_data_n;
	public int test_data_n;
    public int letter_length;
    public double learn_factor;
    public double forget_factor;
    public double error;
    public double out;
    public Scanner scanner;
    public Random random; 
    
    public double[] t_temp;
    public double[][] w_temp;   
    public double[][] l_temp;
    public double[][] test_data;
    
    public Hebbian() {
        learning_data_n = 26;
        test_data_n = 36;
        letter_length = 35;
        learn_factor = 0.01;
        forget_factor = 0.2;
        error = 0;
        
        t_temp= new double[letter_length];
        w_temp = new double[letter_length][learning_data_n];    
        l_temp = new double[letter_length][learning_data_n];
        test_data = new double[letter_length][test_data_n];

    }
    


//blok actywacji
    
    public double activation_Box(double sum){
       double result=0;
       if(sum>0) result=1;
        return result;
        
    }
    
//funkcja uczaca   
    
    public void learn() {
        int epoka=0;
        double sum=0;
        double sum2=0;
        double MAPE=0;
        double MSE=0;
        double global_error=0;
        
        double temp=0;
        scanData(l_temp,learning_data_n,letter_length,"dane_uczace.txt");
        scanData(test_data,test_data_n,letter_length,"dane_testowe.txt");
        setWeights(w_temp);
        
        do{
            System.out.println("Epoka: "+epoka);
            for(int i=0;i<learning_data_n;++i){
                global_error = 0.;
                for(int j=0;j<letter_length;++j){
                    temp = t_temp[j];
                    t_temp[j] = w_temp[j][i]*l_temp[j][i];
                    w_temp[j][i] = w_temp[j][i]*(1-forget_factor)+learn_factor*t_temp[j]*l_temp[j][i];

                    if(error==Math.abs(temp-t_temp[j])) break;
                    error = Math.abs(temp - t_temp[j]);
                    global_error += Math.pow(error,2);

                }

                MSE = Math.pow(global_error,2)/letter_length;
                MAPE = (global_error*10)/letter_length;
                System.out.println("MSE: "+MSE+"\tMAPE: "+MAPE +"\tglobal error: " +global_error);

            }
            epoka++;
        }while(global_error >0.1 && epoka<10000);
    }
    
//funkcja testujaca
    
    public void test(){

    System.out.println("Test");
    double tab[][]=new double[test_data_n][35];
    double a_test[]=new double[35];

    for(int i=0;i<test_data_n;i++){
        for(int j=0;j<letter_length;j++){
            t_temp[j]=w_temp[j][0]*test_data[j][i];

            a_test[j]=activation_Box(t_temp[j]);


        }
       
      
 System.out.println("-----"+(i+1)+"-----");
        
        double[] pom=new double[learning_data_n];
        for(int n=0;n<learning_data_n;n++) pom[n]=0;

        for(int ii=0;ii<learning_data_n;ii++)
            for(int j=0;j<letter_length;j++)
                if(a_test[j]==l_temp[j][ii]) pom[ii]++;
        
        for(int j=0;j<learning_data_n;j++){
        System.out.print((char)(65+j)+": "+(int)(pom[j])+"\t");
        if(j%4==1)
        System.out.println("\n");
        }
    }
    }

    //funkcja pobierająca dane
 
    public void scanData(double[][] dane,int l,int m,String plik){
        try {
            Scanner scanner =new Scanner(new File(plik));
            for(int i=0;i<l;i++){           
                for(int j=0;j<m;j++){  
                    dane[j][i]=scanner.nextInt();
                }


            }
            scanner.close();
        }catch (Exception e){e.printStackTrace();}

    }
    
    //funkcja ustawiająca wagi
    
    public void setWeights(double[][] w){
        Random random=new Random();
        for(int i=0;i<learning_data_n;i++){           
            for(int j=0;j<letter_length;j++){     
               w[j][i]=random.nextDouble()-0.5;
               
            }


        }

    }
                
    //funkcja uruchamiająca program
    
    public void run(){
    	learn();
    	test();   	
    }
}
