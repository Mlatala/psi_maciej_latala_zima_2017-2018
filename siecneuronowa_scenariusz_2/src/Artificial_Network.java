import java.io.File;
import java.nio.file.Path;
import java.util.Random;
import java.util.Scanner;

public class Artificial_Network {
   
	public int learning_data_n;
    public int letter_length;
    public double Weights[];
    public Letter[] Inputs;
    public double fac;
    public double error;
    public double out;
    public Scanner scanner;
    public Random random; 
    
    public Artificial_Network() {
        learning_data_n = 20;
        letter_length = 35;
        fac = 0.1;
        error = 0;
        Weights = new double[letter_length + 1];
        Inputs = new Letter[learning_data_n];
        out = 0;

        for (int i = 0; i < learning_data_n; i++)
            Inputs[i] = new Letter();
    }
    
//pobieranie danych
    
    public void scan_Letter() {
        try {
            scanner = new Scanner(new File("dane_uczace.txt"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Letter l : Inputs) {
            l.scan_Letter_learn(scanner);
        }
    }
    
//ustalanie wag
    
    public void random_Weights() {
        random = new Random();
        for (int i = 0; i < letter_length + 1; i++)
            Weights[i] = random.nextDouble() - 1;

    }
    public void set_Weights(double delta,Letter l){
        for(int i=0;i<letter_length;i++)
            Weights[i]+=delta*fac*l.letter[i];

        Weights[letter_length]=delta*fac;
    }

//blok actywacji
    
    public double activation_Box(double sum){
        double beta=1;
        return(1/(1+Math.exp(-beta*sum)));
    }
    
//funkcja uczaca   
    
    public void learn() {
        this.scan_Letter();
        this.random_Weights();
        int epoka=0;
        double sum=0;
        double sum2=0;
        double MAPE=0;
        double MSE=0;
        double global_error=0;
        do {
            global_error=0;
            for(int i=0;i<learning_data_n;i++){
               sum= Inputs[i].summing_Box(Weights,letter_length);
               out=activation_Box(sum);
               error=(Inputs[i].size-out);
               set_Weights(error,Inputs[i]);
               global_error+=error*error;
               sum2+=sum;
            }
            epoka++;
            MSE=Math.pow(global_error, 2)/learning_data_n;
            MAPE=Math.abs(global_error/sum2);
            System.out.println("Epoka: "+epoka);
            System.out.println("MSE: "+MSE);
            System.out.println("MAPE: "+MAPE);
        } while (global_error>0.01 && epoka<100);
    }

//funkcja testujaca
    
    public void test(){
        Letter[] input_Test=new Letter[20];
        double sum;
        int big=0;
        int small=0;
        try {
            scanner=new Scanner(new File("dane_testowe.txt"));
        }catch (Exception e){e.printStackTrace();}

        for (int i=0;i<20;i++)
                input_Test[i]=new Letter();

        for (int i=0;i<20;i++) {
            input_Test[i].scan_Letter_data(scanner);
        }

        for (int i=0;i<20;i++) {
            sum = input_Test[i].summing_Box(Weights, letter_length);
            if(activation_Box(sum)>0.5){
            	input_Test[i].size=1; 
            	big++;
            }
            else{ 
            	input_Test[i].size=0;
            	small++;
            	}
        }

        for (int i=0;i<20;i++) {
            input_Test[i].show_Letter_data();
        }

        System.out.println("Big letters: "+big+"\nSmall letters: "+small);
    }
 
    //funkcja uruchamiająca program
    
    public void run(){
    	learn();
    	test();   	
    }
}
