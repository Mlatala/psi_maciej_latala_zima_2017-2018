import java.util.Scanner;

public class Letter {
    int board;
    int[] letter;
    double sum;
    int size;


    public Letter(){
        board=35;
        letter=new int[board];
        size=-1;
    }
    
//pobieranie danych
    //uczacych
    
    public void scan_Letter_learn(Scanner s){        
        for (int i=0;i<board;i++)
            letter[i]=s.nextInt();
        size=s.nextInt();
    }
    
    //testowych
    
    public void scan_Letter_data(Scanner s){
        for (int i=0;i<board;i++)
            letter[i]=s.nextInt();
    }
    
//wyswietlanie danych
    //uczacych
    
    public void show_Letter_learn(){
        for(int i=0;i<board;i++) {
            if(i%5==0 && i!=0) 
            	System.out.println();
            System.out.print(letter[i]);
        }
        if(size==1)
        	System.out.println("\nBig letter");
        if(size==0)
        	System.out.println("\nSmall letter");
        System.out.println();
    }
    
    //testowych
    
    public void show_Letter_data(){
        for(int i=0;i<board;i++) {
            if(i%5==0 && i!=0) 
            	System.out.println();
            System.out.print(letter[i]);
        }
        if(size==1)
        	System.out.println("\nBig letter");
        if(size==0)
        	System.out.println("\nSmall letter");
        System.out.println();
    }
    
//blok summujacy
    
    public double summing_Box(double[] weight,int n_weight){
        for(int i=0;i<board;i++)
            sum+=letter[i]*weight[i];

        return sum+weight[n_weight];
    }

}
